# Sprint Grid

Sprint Grid is an organizer of tasks and planning them by date.
You can add or remove tasks and dates, change status of the task.
The removing is available when you hover over the task title or date.
Pay attention to validation restrictions.
You can't create tasks with the similar names and the similar dates.

## Node.js
Make sure You have successfully installed Node.js (we recommend 16.10.0).
Then install dependencies using `npm install`

## Localisation
Make sure You are using `master-en` branch because it has english localisation.
While `master` branch has russian localisation

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.
The app will automatically reload if you change any of the source files.


## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.






