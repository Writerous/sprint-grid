import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { SprintGridModule } from './ui/sprint-grid/sprint-grid.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RootStoreModule } from '@store';
import { registerLocaleData } from '@angular/common';
import localeRu from '@angular/common/locales/ru';
import { MatGridListModule } from '@angular/material/grid-list';

registerLocaleData(localeRu, 'ru');

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		SprintGridModule,
		BrowserAnimationsModule,
		RootStoreModule,
		MatGridListModule,
	],
	providers: [
		{
			provide: LOCALE_ID,
			useValue: 'ru',
		},
	],
	bootstrap: [AppComponent],
})
export class AppModule {}
