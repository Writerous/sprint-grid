import { Injectable } from '@angular/core';
import { Task, Timestamp } from '@models';
import { Observable, of } from 'rxjs';

const initialDates: Timestamp[] = [new Date(2020, 5, 1).getTime(), new Date(2020, 4, 2).getTime()];

const initialTasks: Task[] = [
	{
		name: 'Task 1',
		statusMap: new Map([[initialDates[0], 'В работе']]),
	},
	{
		name: 'Task 2',
		statusMap: new Map([[initialDates[1], 'Блокировано']]),
	},
	{
		name: 'Task 3',
		statusMap: new Map([
			[initialDates[0], 'В работе'],
			[initialDates[1], 'Блокировано'],
		]),
	},
];

const preferredStatuses = ['Взять', 'В работе', 'Тестирование', 'Готово', 'Блокировано'];

@Injectable({
	providedIn: 'root',
})
export class SprintGridService {
	public preferredStatuses = preferredStatuses;

	public loadTasks(): Observable<Task[]> {
		return of(initialTasks);
	}
}
